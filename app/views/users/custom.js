$(function () {
    // 引き金となる要素を設定
    var triggerNode = $(".answer");
    var point1 = $(".point1-logo");
    var point2 = $(".point2-logo");
    var point3 = $(".point3-logo");
    // 画面スクロール毎に判定を行う
    $(window).scroll(function () {
        // 引き金となる要素の位置を取得
        var triggerNodePosition = $(triggerNode).offset().top - $(window).height();

        // 現在のスクロール位置が引き金要素の位置より下にあれば‥
        if ($(window).scrollTop() > triggerNodePosition + 30) {
            // なんらかの命令を実行
            jQuery("#answer_text").fadeIn();
            setTimeout(function () {
                jQuery(".bar").addClass('active');
            },300);
            setTimeout(function () {
                jQuery(".accent1").fadeIn();
            }, 800);
            setTimeout(function () {
                jQuery(".accent2").fadeIn();
            }, 1200);
            setTimeout(function () {
                jQuery(".accent3").fadeIn();
            }, 1700);

        }

        var point1Position = $(point1).offset().top - $(window).height();
        if ($(window).scrollTop() > point1Position + 30) {
            // なんらかの命令を実行

            setTimeout(function () {
                jQuery(".point1-logo").addClass('point-after');
            }, 500);
        }else{
            jQuery(".point1-logo").removeClass('point-after');
        }
        var point2Position = $(point2).offset().top - $(window).height();
        if ($(window).scrollTop() > point2Position + 30) {
            // なんらかの命令を実行
            setTimeout(function () {
                jQuery(".point2-logo").addClass('point-after');
            }, 500);
        } else {
            jQuery(".point2-logo").removeClass('point-after');
        }
        var point3Position = $(point3).offset().top - $(window).height();
        if ($(window).scrollTop() > point3Position + 30) {
            // なんらかの命令を実行
            setTimeout(function () {
                jQuery(".point3-logo").addClass('point-after');
            }, 500);
        } else {
            jQuery(".point3-logo").removeClass('point-after');
        }
    });

    $('a[href^=#]').click(function () {
        // スクロールの速度
        var speed = 1000; // ミリ秒
        // アンカーの値取得
        var href = $(this).attr("href");
        // 移動先を取得
        var target = $(href == "#" || href == "" ? 'html' : href);
        // 移動先を数値で取得
        var position = target.offset().top;
        // スムーススクロール
        $('body,html').animate({scrollTop: position}, speed, 'swing');
        return false;
    });
});