class PostsController < ApplicationController
	def create
		params[:post][:send_user_id] = current_user.id
		
		# get user_id from user show page. if top page, it has
		# params[:post][:user_id] = URI(request.referer).path.delete("/users/") if params[:post][:user_id].nil?
		
		@post = Post.new(post_params)
		if @post.save
			# make push data
			Push.create(user_id: params[:post][:user_id])
			
			redirect_to URI(request.referer).path, :notice => "Q&Aを投稿しました。"
		else
			render user_path(params[:post][:user_id])
		end
	end
	
	def posts_send
		@user = User.find(current_user.id)
		@posts = Post.where("send_user_id = ?", current_user.id).page(params[:page]).per(10).order("updated_at DESC")
		@is_my_page = true if current_user.id.to_i == @user.id.to_i # my page can see send_message
		@this_page_params_user_id = @user.id.to_i # for hidden value of user_id to send message
		@send = 'send' # difference from users/top
	end
	
	def edit
		@post = Post.find_obfuscated(params[:id])
	end
	
	def update
		@post = Post.find_obfuscated(params[:id])
		if @post.update(post_params)
			flash[:success] = "投稿を更新しました。"
			redirect_to user_path(current_user)
		else
			render 'edit'
		end		
	end
	
	private
		def post_params
			accessible = [:user_id, :send_user_id, :content, :anonymous, :nickname]
			
			params.require(:post).permit(accessible)
		end
		
		def push_params
			accessible = [:user_id]
			params.require(:push).permit(accessible)
		end
end