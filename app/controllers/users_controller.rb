class UsersController < ApplicationController
	
	before_action :set_user, only: [:show, :edit, :update, :destroy, :edit_detail, :save_from_twitter]
	
	def before_login
		
	end
	
	def show
		redirect_to root_path if @user.open == 1
		@posts = @user.posts.page(params[:page]).per(10).order("updated_at DESC")
		@post = Post.new
		
		@is_my_page = true if current_user.id.to_i == @user.id.to_i
		@this_page_params_user_id = @user.id.to_i
		
		# delete push
		if params[:id].to_i == current_user.id.to_i
			Push.destroy_all(user_id: current_user.id)
			session[:push_count] = nil
		end
	end

	def index
	end

	def edit
		@edu_records = @user.edu_records
		@arbeit_records = @user.arbeit_records
		@applicant = @user.applicant
		
		# 新規作成用
		@user.images.build unless @user.images.any?
		@user.build_applicant if @applicant.nil?
		
		(3 - @edu_records.count ).times {
			@user.edu_records.build
		} unless @edu_records.any?
		
		(4 - @arbeit_records.count).times {
			@user.arbeit_records.build
		} unless @arbeit_records.any?
	end
	
	def edit_detail
		@wish_jobs = @user.wish_jobs
		@applicant = @user.applicant
		
		@status = @user.status
		@user.build_status if @status.nil?
		
		# 新規作成
		@user.build_applicant if @applicant.nil?
		
		(3 - @wish_jobs.count ).times {
			@user.wish_jobs.build
		} unless @wish_jobs.any?
	end
	
	# 後回し	
	def create_record
		@user = User.find_obfuscated(current_user.id)

		self.edit
		@user.edu_records.build
		#respond_to do |format|
		#format.html {render :file => "users/edit"}
		#end
	end
	
	def top
		if current_user
			@user ||= current_user
			
			# 初めに投稿を促す
			@have_advertising = @user.advertising
			unless @have_advertising.any?
				@advertising = Advertising.new
				@advertising.build_category
			end
			
			@users = User.where.not(id: current_user.id)
			@users = @users.where(open: 0)
			@post = Post.new
		else
			render 'before_login'
		end
	end
	
	def from_twitter
		@user = User.find_obfuscated(params[:id])
	end
	
	def save_from_twitter
		if @user.update(user_params)
			flash[:success] = "確認メールを、登録したメールアドレス宛に送信しました。メールに記載されたリンクを開いてアカウントを有効にして下さい。"
			redirect_to root_path
		else
			render 'from_twitter'
		end		
	end

	def update
		_params = user_params

		self.save_sub_image

		if _params.present? && @user.update(user_params)
			flash[:success] = "Profile updated"
			redirect_to @user
		else
			render 'edit'
		end
	end

	def finish_signup

	end

	def save_sub_image
		# imagesの登録
		unless params[:images].nil?
			params[:images].each { |image|
				@user.images.create(image: image)
			}
		end
	end

	private

	def set_user
		begin
			@user = User.find_obfuscated(params[:id])
		rescue
			redirect_to root_path
		end
	end

	def user_params
		accessible = [ :family_name,:first_name, :email, :birthday, :open, :gender,
			
			 applicant_attributes: [:address_pref, :address_city, :self_apeal, :belonging, :monday_ok_flag,
			 	:monday_ok_from_time, :monday_ok_to_time, :tuesday_ok_flag, :tuesday_ok_from_time, :tuesday_ok_to_time,
			 	:wednesday_ok_flag, :wednesday_ok_from_time, :wednesday_ok_to_time, :thursday_ok_flag, :thursday_ok_from_time,
			 	:thursday_ok_to_time, :friday_ok_flag, :friday_ok_from_time, :friday_ok_to_time, :saturday_ok_flag, :saturday_ok_from_time,
			 	:saturday_ok_to_time, :sunday_ok_flag, :sunday_ok_from_time, :sunday_ok_to_time, :gakko_name , :self_introduce, :id ],
			 	
			 edu_records_attributes: [:name, :from_year, :from_month, :to_year, :to_month, :id ],
			 
			 arbeit_records_attributes: [:name, :from_year, :from_month, :to_year, :to_month, :id ],
			 
			 status_attributes: [:id, :status],
			 
			 wish_jobs_attributes: [:wish_job_name, :wish_payment, :wish_placement, :id]]
			 #images_attributes: [:id, :image_file_name, :image_content_type, :image_file_size, :image_update_at, :imageable_id, :imageable_type] ]
			 
		accessible = [ :picture ] if params[:user].present? && params[:user][:picture].present?
		#raise
		unless params[:user].blank?
			accessible << [ :password, :password_confirmation ]
			params.require(:user).permit(accessible)
		end
	end
end
