class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # for devise save
  before_action :configure_permitted_parameters, if: :devise_controller?
  # AWS.config(access_key_id: 'AKIAJL7A7L2I3QIVTIIQ', secret_access_key: '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', region: 'ap-northeast-1')
  
  # ログイン後でないとコンテンツの表示をさせない
  def ensure_signup_complete
  	return if action_name == 'finish_signup'
  	if current_user && !current_user.email_verified?
  		redirect_to finish_signup_path(current_user)
  	end
  end
  
  def configure_permitted_parameters
  	devise_parameter_sanitizer.for(:sign_in) << [:family_name,:first_name, :birthday, :gender, :email]
   	devise_parameter_sanitizer.for(:sign_up) << [:family_name,:first_name, :birthday, :gender, :email]
  	devise_parameter_sanitizer.for(:account_update) << [:family_name,:first_name, :birthday, :gender, :email]

  end
  
  def after_sign_in_path_for(resource)
  	root_path
  end
end
