class RegistrationsController < Devise::SessionsController

	def new
		raise
	end

	private

	def user_params
		params.require(:user).permit(:avatar, :name, :introduction, :birth_year, :birth_month,
		:belong, :address, :goal, :images)
	end

end