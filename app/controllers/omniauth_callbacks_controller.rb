class OmniauthCallbacksController < Devise::OmniauthCallbacksController
	def self.provides_callback_for(provider)
		class_eval %Q{
			def #{provider}
				@user = User.find_for_oauth(env["omniauth.auth"], current_user)

				if @user.persisted?
					if @user.email.include?("change@me")
						redirect_to from_twitter_path(@user)	and return
					end
					sign_in_and_redirect @user, event: :authentication
					# set_flash_message(:notice, :success, kind: "#{provider}".capitalize) if is_navigational_format?
				else
					session["devise.#{provider}?data"] = env["omniauth.auth"]
					redirect_to new_user_registraion_url
				end
			end
		}
	end

	[:twitter, :facebook].each do |provider|
		provides_callback_for provider
	end

=begin
def after_sign_in_path_for(resource)
if resource.email_verified?
super resource
else
finish_signup_path(resource)
end
end
=end
end
