class AdvertisingController < ApplicationController

	before_action :set_user, only: [:top, :new, :search_result]

	def top
		@_advertising = Advertising.new #category作成用
		@_advertising.build_category
		
		@advertising = Advertising.page(params[:page]).per(10).order("updated_at DESC")
	end

	def new
		@advertising = Advertising.new
		@advertising.build_category
	end
	
	def update
		params[:advertising][:category_id] = params[:advertising][:category][:category]
		@advertising = Advertising.find_obfuscated(params[:id])
		if @advertising.update(advertising_params)
			flash[:success] = "口コミを更新しました。"
			redirect_to advertising_top_path
		else
			render 'edit'
		end		
	end
	
	def edit
		@advertising = Advertising.find_obfuscated(params[:id])
		@advertising.build_category
	end

	def search
		@search = ""
		if params[:search].present? || params[:shop_pref].present? || params[:shop_city].present? || params[:category][:category].present?
			@search = Advertising.search(params)
		end

		if params[:json] == 'json'
			respond_to do |format|
				format.json{render:json => @search}
			end
		end
		
		@msg = '検索結果は０件でした。' if @search.blank?
	end
	
	def search_result
		@_advertising = Advertising.new #category作成用
		@_advertising.build_category
		
		@advertising = Advertising.param_parser(params)
		@advertising = @advertising.page(params[:page]).per(10).order("updated_at DESC")
	end

	def create
		
		params[:advertising][:category_id] = Advertising.get_category(params[:advertising][:category][:category])
		params[:advertising][:user_id] = current_user.id
		
		@advertising = Advertising.new(advertising_params)
		if @advertising.save
			redirect_to advertising_top_path, :notice => "口コミに投稿しました。"
		else
			@advertising.build_category
			render 'new'
		end
	end

	private

	def set_user
		if current_user.present?
			@user = User.find(current_user.id)
		else
			redirect_to root_path
		end
	end
	
	def advertising_params
		accessible = [:user_id, :content, :ranking, :environment, :friends, :busy, :money, :worthwhile, :flexible, :shop_pref, :shop_city, :shop_name, :category_id, :anonymous, :nickname,
			category_attributes: [:id, :category, :advertising_id]]
		params.require(:advertising).permit(accessible)
	end
end
