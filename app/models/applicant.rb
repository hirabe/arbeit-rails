class Applicant < ActiveRecord::Base
	belongs_to :user
	validates :self_introduce,
		length: { maximum: 250 }
end
