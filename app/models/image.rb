class Image < ActiveRecord::Base
	belongs_to :imageable, :polymorphic => true
	
	has_attached_file :image,
		:style => {
  		:thumb => "100×100",
  		:medium => "200×200",
  		:large => "300×300",
		},
		:storage => :s3,
		:s3_permissions => :private,
		:s3_credentials => "#{Rails.root}/config/s3.yml",
		:url => "s3_domain_url",
  	:hash_secret => "fjwoefjc4902rj3jgojmfdsf9wfwvzagjl",
  	:path => ":attachment/:id/:hash.:extension"
	validates_attachment_size :image, :less_than => 1.megabytes, :attachment_presence => true
	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
