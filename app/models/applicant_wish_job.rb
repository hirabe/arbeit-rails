class ApplicantWishJob < ActiveRecord::Base
	belongs_to :user
	validates :wish_job_name,
		length: { maximum: 255 }
	validates :wish_payment,
		numericality: { only_integer: true, less_than: 5000, allow_blank: true }
	validates :wish_placement,
		length: { maximum: 255 }
end
