class ApplicantEduRecord < ActiveRecord::Base
	belongs_to :user
	validates :from_year,
 		length: {is: 4, allow_blank: true},
 		numericality: { only_integer: true, allow_blank: true }
	validates :from_month,
 		length: {within: 1..2, allow_blank: true},
 		numericality: { only_integer: true, allow_blank: true }
	validates :to_year,
 		length: {is: 4, allow_blank: true},
 		numericality: { only_integer: true, allow_blank: true }
	validates :to_month,
 		length: {within: 1..2, allow_blank: true},
 		numericality: { only_integer: true, allow_blank: true }
	validates :name,
 		length: { maximum: 255, allow_blank: true }
end
