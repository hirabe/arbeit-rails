class Advertising < ActiveRecord::Base
	obfuscatable
	# category
	has_one :category, dependent: :destroy, class_name: 'Category'
	accepts_nested_attributes_for :category
	
	# user
	belongs_to :user
	
	validates :shop_name,
		presence: true,
		length: {maximum: 250, allow_blank: true}
	validates :shop_pref,
		presence: true
	validates :shop_city,
		presence: true
	validates :content,
		length: {minimum: 100}
	
	def Advertising.get_category(val = nil)
		category = Category.all
		
		category.each do |c|
			if c.category == val
				return c.id
			end
		end
	end
	
	def Advertising.search(params)
		
		unless params[:json] == 'json' # 配列を文字に変換
			params[:shop_pref] = params[:shop_pref].join(" ") if params[:shop_pref].present?
			params[:shop_city] = params[:shop_city].join(" ") if params[:shop_city].present?
		end
		
		@search = Advertising.select('shop_name', 'shop_pref', 'shop_city').all
		
		if params[:search].present?
			
			@hiragana = params[:search].tr("ア-ン", "あ-ん")
			@katakana = params[:search].tr("あ-ん", "ア-ン")
			@search = @search.where(("shop_name LIKE ? OR shop_name LIKE ?"), "%#{@hiragana}%","%#{@katakana}%")
		end

		@search = @search.where("shop_pref = ?", "#{params[:shop_pref]}") if params[:shop_pref].present?
		@search = @search.where("shop_city = ?", "#{params[:shop_city]}") if params[:shop_city].present?
		
		category = self.get_category(params[:category][:category]) if params[:category][:category].present?
		@search = @search.where("category_id = ?", "#{category}") if category.present?
		
		if params[:json] == 'json'
			return @search.group("shop_name") 
		else
			return @search.group("shop_name", 'shop_pref', 'shop_city') 
		end
	end
	
	def Advertising.param_parser(params)
		ref_hash = params[:search]
		ref_hash.split("&").map { |v| v.split("=") }
		key_value = Hash[ *ref_hash.split("&").map { |v| v.split("=") }.flatten ]

		Advertising.where("shop_name = ? AND shop_pref = ? AND shop_city = ?", key_value["name"], key_value["pref"], key_value["city"])
	end
end
