class User < ActiveRecord::Base
	obfuscatable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable ,:omniauthable, :timeoutable, :token_authenticatable#, :confirmable
         
  TEMP_EMAIL_PREFIX = 'change@me'
  TEMP_EMAIL_REGEX = /\Achange@me/
  
  # paperclip main_picture
  has_attached_file :picture, 
  	:style => {
  		:thumb => "100×100",
  		:medium => "200×200",
  		:large => "300×300",
  	},
  	:storage => :s3,
  	:s3_permissions => :private,
  	:s3_credentials => "#{Rails.root}/config/s3.yml",
  	:url => "s3_domain_url",
  	:hash_secret => "fjwoefjc4902rj3jgojmfdsf9wfwvzagjl",
  	:path => ":attachment/:id/:hash.:extension"
  validates_attachment_size :picture, :less_than => 1.megabytes, :attachment_presence => true
  validates_attachment_content_type :picture, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  
  #paperclip sub_picutres
	has_many :images, :as => :imageable, dependent: :destroy
	accepts_nested_attributes_for :images
	
	#applicant
	has_one :applicant, dependent: :destroy
	accepts_nested_attributes_for :applicant
	
	#applicant_edu_records
	has_many :edu_records, dependent: :destroy, class_name: 'ApplicantEduRecord'
	accepts_nested_attributes_for :edu_records
	
	#applicant_arbeit_records
	has_many :arbeit_records, dependent: :destroy, class_name: 'ApplicantArbeitRecord'
	accepts_nested_attributes_for :arbeit_records
	
	#applicant_wish_jobs
	has_many :wish_jobs, dependent: :destroy, class_name: 'ApplicantWishJob'
	accepts_nested_attributes_for :wish_jobs
	
	#applicant_status
	has_one :status, dependent: :destroy, class_name: 'ApplicantStatus'
	accepts_nested_attributes_for :status
	
	#advertising
	has_many :advertising
	
	#post
	has_many :posts
	
	#push
	has_many :pushes
  
  validates_format_of :email, :without => TEMP_EMAIL_REGEX, on: :update
  validates :family_name,
  	length: { maximum: 10 }
  validates :first_name,
  	length: { maximum: 10 }
 	validates_uniqueness_of :email
 	validates_presence_of(:family_name)
 	validates_presence_of(:first_name)
  
  def self.find_for_oauth(auth, signed_in_resource = nil)
  	
  	identity = Identity.find_for_oauth(auth)
  	user = signed_in_resource ? signed_in_resource : identity.user

  	if user.nil?
  		email_is_verified = auth.info.email # && (auth.info.verified || auth.info.verified_email)
  		email = auth.info.email if email_is_verified
  		user = User.where(:email => email).first if email

  		# facebook/twitter
  		if user.nil?
  			user = User.new(
  			family_name: auth.extra.raw_info.last_name.present? ? auth.extra.raw_info.last_name : 'pending',
  			first_name: auth.extra.raw_info.first_name.present? ? auth.extra.raw_info.first_name : 'pending',
  			email: email ? email : "#{TEMP_EMAIL_PREFIX}-#{auth.uid}-#{auth.provider}.com",
  			password: Devise.friendly_token[0,20],
  			gender: (auth.extra.raw_info.gender == "male" || auth.extra.raw_info.gender == "男性" || auth.extra.raw_info.gender.nil? ) ? "男性" : "女性",
  			confirmed_at: Time.now.to_s(:db)
  			)
  			
  			#user.skip_confirmation!
  			user.save!
  			
  		end
  	end
  	
  	if identity.user != user
  		identity.user = user
  		identity.save!
  	end
  	user
  end
  
  def email_verified?
  	self.email && self.email !~ TEMP_EMAIL_REGEX
  end
end
