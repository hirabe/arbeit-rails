$(document).ready(function() {
    //市町村検索
    //初期設定
    var area_pref = $('.area_pref');
    var default_pref = area_pref.val();
    //都道府県が入るselect
    var area_city = $('.area_city');
    var defalut_city = area_city.val();

    //市町村が入るselect

    //最初に都道府県を読み込む
    $.getJSON('http://search.olp.yahooapis.jp/OpenLocalPlatform/V1/addressDirectory?callback=?', {
        appid : 'U9lIU0axg65qj6qztfqFQ4bXem1vHWYbz3pLT_7smKhbX0r8pR82bKu07Dhf.nGe',
        ac : 'JP',
        output : 'json'
    }, function(json) {
        area_pref.children().remove();
        //一つ目のoption(選択してください）のみ残して削除
        $.each(json.Feature[0].Property.AddressDirectory, function(key, value) {
            if (this.AreaCode == "08" || this.AreaCode == "09" || this.AreaCode == "10" || this.AreaCode == "11" || this.AreaCode == "12" || this.AreaCode == "13" || this.AreaCode == "14") {
                var txt = String(this.Name);
                var code = String(this.AreaCode);
                area_pref.append('<option value="' + code + '">' + txt + "</option>");
            }
        });
        area_pref.val(default_pref).change();
        // 都道府県のデフォルト値
    });

    //都道府県から市町村を検索
    area_pref.on('change', function() {
        $.getJSON('http://search.olp.yahooapis.jp/OpenLocalPlatform/V1/addressDirectory?callback=?', {
            appid : 'U9lIU0axg65qj6qztfqFQ4bXem1vHWYbz3pLT_7smKhbX0r8pR82bKu07Dhf.nGe',
            ac : $(this).val(),
            output : 'json'
        }, function(json) {
            area_city.children().remove();
            //一つ目のoption(選択してください）のみ残して削除
            $.each(json.Feature[0].Property.AddressDirectory, function(key, value) {
                var txt = String(this.Name);
                area_city.append('<option value="' + txt + '">' + txt + "</option>");
            });
            area_city.val(defalut_city);
            // 市町村のデフォルト値
        });
    });

    // 追加ボタン
    $('#edu-add').click(function() {
        $.ajax({
            url : "/user/create_record",
            type : 'post',
            data : {
                store : {
                    type : 'edu'
                }
            },
            success : function(data) {
                return false;
            },
            error : function(data) {
                console.log(data);
            }
        });
    });

    $('#new-advertising-search').click(function() {

        $('#searched-comment').empty();
        $('#shop-names').empty();

        if ($('#advertising_shop_pref').val() == null || $('#advertising_shop_city').val() == null || $('#search').val() == null) {
            $('#searched-comment').append('アルバイト先の場所、名称を入力して検索して下さい');
            return false;
        }

        $.ajax({
            url : "/advertising/search",
            type : "post",
            data : {
                category : {
                    category : $('#advertising_category_category').val()
                },
                shop_pref : $('#advertising_shop_pref').val(),
                shop_city : $('#advertising_shop_city').val(),
                search : $('#search').val(),
                json : 'json'
            },
            success : function(data) {
                if (data == "") {
                    $('#searched-comment').append('見つかりませんでした。新規に入力して下さい。');
                } else if (data == 'too-much') {
                    $('#searched-comment').append('範囲を絞り込んで検索して下さい。');
                    return false;
                } else {
                    Object.keys(data).forEach(function(key) {
                        html = '<a href="#"><span class="selected-shop-name">' + data[key]["shop_name"] + '</span></a>';
                        $('#shop-names').append(html);
                    });
                }

                $('#input-shop-name').css("display", "block");

                return false;
            },
            error : function(data) {
                $('#searched-comment').append('見つかりませんでした。新規に入力して下さい。');
                $('#input-shop-name').css("display", "block");
            }
        });
        return false;
    });

    $('.message').click(function() {
        $('#post_user_id').val($(this).prev().children().attr('href').replace("/users/", ""));
    });

    // 文字カウント
    var textareaCountMax = 100;
    $('#advertising_content').bind('keydown keyup keypress change', function() {
        var thisValueLength = $(this).val().length;
        $('.count_textarea').html(thisValueLength);
    });

    $(window).load(function() {
        $('.count_textarea').html(textareaCountMax);
    });

    // 星
    $('#edit-star').raty({
        size : 36,
        starOff : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-off.png',
        starOn : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-on.png',
        score : $('#advertising_update_ranking').val(),
        scoreName : 'advertising[ranking]',
    });
    $('#edit-star-environment').raty({
        size : 36,
        starOff : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-off.png',
        starOn : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-on.png',
        score : $('#advertising_update_environment').val(),
        scoreName : 'advertising[environment]'
    });
    $('#edit-star-flexible').raty({
        size : 36,
        starOff : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-off.png',
        starOn : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-on.png',
        score : $('#advertising_update_flexible').val(),
        scoreName : 'advertising[flexible]'
    });
    $('#edit-star-busy').raty({
        size : 36,
        starOff : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-off.png',
        starOn : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-on.png',
        score : $('#advertising_update_busy').val(),
        scoreName : 'advertising[busy]'
    });
    $('#edit-star-money').raty({
        size : 36,
        starOff : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-off.png',
        starOn : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-on.png',
        score : $('#advertising_update_money').val(),
        scoreName : 'advertising[money]'
    });
    $('#edit-star-worthwhile').raty({
        size : 36,
        starOff : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-off.png',
        starOn : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-on.png',
        score : $('#advertising_update_worthwhile').val(),
        scoreName : 'advertising[worthwhile]'
    });
    $('#edit-star-friends').raty({
        size : 36,
        starOff : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-off.png',
        starOn : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-on.png',
        score : $('#advertising_update_friends').val(),
        scoreName : 'advertising[friends]'
    });

    if (document.getElementById("advertising-modal") != null) {
        $('#advertising-modal').modal();
        if ($(window).width() >= 768) {
            $('.modal-dialog').css("width", "1000px");
        }
    }

});
$(document).on('click', '.selected-shop-name', function() {
    $('#advertising_shop_name').val($(this).text());
    return false;
});
$(document).on('click', '.shop-name', function() {
    window.location.href = '/advertising/search_result/' + $(this).attr('rel');
    return false;
});
$(document).on('click', '#anonymous_button', function(){
     $('#nickname-filed').css("display", "block");
});
$(document).on('click', '#username_button', function(){
    $('#nickname-filed').css("display", "none");
});