// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require turbolinks
//= require jquery.turbolinks
//= require jquery.raty
//= require_tree .

$(function() {
    $('.timekeper').timepicker({
        showSeconds : false,
        showMeridian : false,
        defaultTime : false,
        minuteStep : 30,
        disableFocus : true
    });

    $('#star').raty({
        size : 36,
        starOff : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-off.png',
        starOn : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-on.png',
        scoreName : 'advertising[ranking]',
    });
    $('#star-environment').raty({
        size : 36,
        starOff : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-off.png',
        starOn : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-on.png',
        scoreName : 'advertising[environment]'
    });
    $('#star-flexible').raty({
        size : 36,
        starOff : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-off.png',
        starOn : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-on.png',
        score : $('#advertising_update_flexible').val(),
        scoreName : 'advertising[flexible]'
    });
    $('#star-busy').raty({
        size : 36,
        starOff : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-off.png',
        starOn : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-on.png',
        scoreName : 'advertising[busy]'
    });
    $('#star-money').raty({
        size : 36,
        starOff : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-off.png',
        starOn : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-on.png',
        scoreName : 'advertising[money]'
    });
    $('#star-worthwhile').raty({
        size : 36,
        starOff : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-off.png',
        starOn : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-on.png',
        scoreName : 'advertising[worthwhile]'
    });
    $('#star-friends').raty({
        size : 36,
        starOff : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-off.png',
        starOn : 'https://s3-ap-northeast-1.amazonaws.com/arbeit-static/star-on.png',
        scoreName : 'advertising[friends]'
    });
});
