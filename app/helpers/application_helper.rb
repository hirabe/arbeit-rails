module ApplicationHelper
	include Const
	def get_address_name(user = nil)
		if user.applicant.present? && user.applicant.address_pref.present?
			address = get_prefecture[user.applicant.address_pref]
			address << user.applicant.address_city
		end
	end
	
	def get_pref_name(pref = nil)
		get_prefecture[pref]
	end

	def get_age(user = nil)
		(Date.today.strftime("%Y%m%d").to_i - user.birthday.strftime("%Y%m%d").to_i) / 10000 if user.birthday.present?
	end

	def applicant
		@user.applicant
	end

	def get_applicant_data(user = nil, column = nil)
		user.applicant.send(column) if user.applicant.present?
	end

	def get_arbeit_records
		record_list = ""
		if @user.arbeit_records.present?
			@user.arbeit_records do |record|
				record_list += %Q{
         <li style="margin-bottom:5px;border-bottom: 1px solid #ececec;">
          <span>アルバイト履歴：#{a_record.name + "　"}
           #{record.from_year + "年" + record.from_month + "月"}〜
           #{record.to_year + "年" + record.to_month + "月"}
           </span>
        </li>
			}
			end
		end
		@arbeit_records = record_list.html_safe
	end

	def get_wish_jobs
		record_list = ""
		jobs = @user.wish_jobs
		if jobs.present?
			jobs.each do |record|
				record_list += %Q{
			 <li style="margin-bottom:5px;border-bottom: 1px solid #ececec;">
        <span>希望のバイト：#{record.wish_job_name} #{record.wish_placement} #{record.wish_payment} </span>
       </li>
			}
			end
		end
		@wish_jobs = record_list.html_safe
	end
	
	def get_main_image
		image = '<a data-toggle="modal" href="#modal-picture" class="modal-picture">'
		
		if @user.picture.path.present?
			image += image_tag("https://s3-ap-northeast-1.amazonaws.com/arbeit-static/" + @user.picture.path, data: {toggle: 'modal'}, data: {target: '#img_modal'}, :id => "main-picture", :style => "width: 200px; height:100%;")
		else
			image += image_tag("default_image.png", data: {toggle: 'modal'}, data: {target: '#img_modal'}, :id => "main-picture", :style => "width: 200px; height:100%;margin-bottom:20px;")
		end
		
		image += '</a>'
		@main_image = image.html_safe
	end

	def get_sub_images
		images = ""

		if @user.images.present?
			@user.images.each do |pic|
				images += '<a data-toggle="modal" href="#modal-picture" class="modal-picture">'
				images += image_tag("https://s3-ap-northeast-1.amazonaws.com/arbeit-static/" + pic.image.path, :style => "width: 90px; height:100%;paddign-right:5px;padding-bottom:5px;")
				images += '</a>'
			end
		end
		@sub_images = images.html_safe
	end
	
	def get_user_main_image(user = nil)
		
		if user.picture.path.present?
			image = link_to image_tag("https://s3-ap-northeast-1.amazonaws.com/arbeit-static/" + user.picture.path, :class => 'user-list-image' ), user_path(user)
		else
			image = link_to image_tag("default_image.png", :class => 'user-list-image') , user_path(user)
		end
		@user_pic = image
	end
	
	def get_user_name_with_link(user = nil)
		name = user.family_name + " " + user.first_name
		link_to name, user_path(user)
	end
end
