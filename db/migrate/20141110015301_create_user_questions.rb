class CreateUserQuestions < ActiveRecord::Migration
	# QA table
  def change
    create_table :user_questions do |t|
      t.integer :user_id
      t.integer :open_flag # 匿名か否か
      t.text :comment

      t.timestamps
    end
    add_index :user_questions, :user_id
  end
end
