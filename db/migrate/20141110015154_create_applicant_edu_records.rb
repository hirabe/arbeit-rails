class CreateApplicantEduRecords < ActiveRecord::Migration
	# 履歴情報
  def change
    create_table :applicant_edu_records do |t|
      t.integer :user_id
      t.string :name
      t.integer :from_year # 履歴開始年
      t.integer :from_month # 履歴開始月
      t.integer :to_year # 履歴終了年
      t.integer :to_month # 履歴終了月

      t.timestamps
    end
    add_index :applicant_edu_records, :user_id
  end
end
