class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.references :user, index: true # 受け取る側のuser_id
      t.integer :send_user_id, index: true #送信する側のuser_id
      t.text :content
      t.integer :anonymous, :default => 0 # 0公開 1非公開
      t.string :nickname

      t.timestamps
    end
  end
end
