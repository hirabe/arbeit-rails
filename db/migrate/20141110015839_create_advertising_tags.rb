class CreateAdvertisingTags < ActiveRecord::Migration
	# 口コミ概要情報
  def change
    create_table :advertising_tags do |t|
      t.integer :advertising_contents_id
      t.string :pref # 店舗該当県
      t.string :city # 店舗該当市町村
      t.string :name # 店舗名

      t.timestamps
    end
    add_index :advertising_tags, :advertising_contents_id
  end
end
