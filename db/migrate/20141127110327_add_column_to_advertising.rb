class AddColumnToAdvertising < ActiveRecord::Migration
  def change
  	add_column :advertising, :environment, :integer
  	add_column :advertising, :friends, :integer
    add_column :advertising, :busy, :integer
    add_column :advertising, :money, :integer
    add_column :advertising, :worthwhile, :integer
    add_column :advertising, :flexible, :integer
  end
end
