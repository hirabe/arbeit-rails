class AddImageableToImage < ActiveRecord::Migration
  def change
    add_reference :images, :imageable, index: true
    add_column :images, :imageable_type, :string
  end
end
