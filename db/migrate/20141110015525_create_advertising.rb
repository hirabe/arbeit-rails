class CreateAdvertising < ActiveRecord::Migration
	# 口コミ情報
  def change
    create_table :advertising do |t|
      t.integer :user_id
      t.text :content
      t.integer :ranking # 1〜5
      t.string :shop_pref
      t.string :shop_city
      t.string :shop_name
      t.integer :anonymous #1は匿名
      t.string :nickname

      t.integer :category_id, index: true
      
      t.timestamps
    end
    add_index :advertising, :user_id
  end
end
