class CreateApplicants < ActiveRecord::Migration
 # 応募者の情報
  def change
    create_table :applicants do |t|
      t.integer :user_id
      t.string :belonging # 所属　大学名や高校名など
      t.string :gakko_name
      t.string :address_pref #　居住地　県
      t.string :railway_line
      t.string :address_city
      t.integer :applicant_status
      t.string :self_introduce
      t.text :self_apeal # 自己アピール欄　履歴書を元に作成
      t.integer :monday_ok_flag # 働ける日　OKの場合フラグ1
      t.integer :monday_ok_from_time # 働ける日from（から）の時間
      t.integer :monday_ok_to_time # 働ける日to(まで)の時間
      t.integer :tuesday_ok_flag
      t.integer :tuesday_ok_from_time
      t.integer :tuesday_ok_to_time
      t.integer :wednesday_ok_flag
      t.integer :wednesday_ok_from_time
      t.integer :wednesday_ok_to_time
      t.integer :thursday_ok_flag
      t.integer :thursday_ok_from_time
      t.integer :thursday_ok_to_time
      t.integer :friday_ok_flag
      t.integer :friday_ok_from_time
      t.integer :friday_ok_to_time
      t.integer :saturday_ok_flag
      t.integer :saturday_ok_from_time
      t.integer :saturday_ok_to_time
      t.integer :sunday_ok_flag
      t.integer :sunday_ok_from_time
      t.integer :sunday_ok_to_time

      t.timestamps
    end
    add_index :applicants, :user_id
  end
end
