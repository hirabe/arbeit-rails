class CreateApplicantArbeitRecords < ActiveRecord::Migration
  def change
    create_table :applicant_arbeit_records do |t|
      t.integer :user_id
      t.string :name
      t.integer :from_year
      t.integer :from_month
      t.integer :to_year
      t.integer :to_month

      t.timestamps
    end
    add_index :applicant_arbeit_records, :user_id
  end
end
