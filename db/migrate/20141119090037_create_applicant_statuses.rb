class CreateApplicantStatuses < ActiveRecord::Migration
  def change
    create_table :applicant_statuses do |t|
      t.references :user, index: true
      t.string :status

      t.timestamps
    end
  end
end
