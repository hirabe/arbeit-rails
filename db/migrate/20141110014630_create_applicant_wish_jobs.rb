class CreateApplicantWishJobs < ActiveRecord::Migration
	 # 応募者の希望職種
  def change
    create_table :applicant_wish_jobs do |t|
      t.integer :user_id
      t.string :wish_job_name # 希望職種名
      t.integer :wish_payment # 希望金額（時給）
      t.string :wish_placement # 希望地（自由記入）

      t.timestamps
    end
    add_index :applicant_wish_jobs, :user_id
  end
end
