class AddSomeFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :gender, :string
    add_column :users, :birthday, :date
    add_column :users, :ower_flag, :integer, :default => 0 #店長(1)か求職者(0)か
    add_column :users, :family_name, :string
    add_column :users, :first_name, :string
    add_column :users, :open, :integer, :default => 0 # 0公開 1非公開
  end
end
