class CreatePushes < ActiveRecord::Migration
  def change
    create_table :pushes do |t|
      t.references :user, index: true

      t.timestamps
    end
  end
end
