# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141127110327) do

  create_table "advertising", force: true do |t|
    t.integer  "user_id"
    t.text     "content"
    t.integer  "ranking"
    t.string   "shop_pref"
    t.string   "shop_city"
    t.string   "shop_name"
    t.integer  "anonymous"
    t.string   "nickname"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "environment"
    t.integer  "busy"
    t.integer  "money"
    t.string   "friends"
    t.string   "flexible"
  end

  add_index "advertising", ["user_id"], name: "index_advertising_on_user_id", using: :btree

  create_table "advertising_tags", force: true do |t|
    t.integer  "advertising_contents_id"
    t.string   "pref"
    t.string   "city"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "advertising_tags", ["advertising_contents_id"], name: "index_advertising_tags_on_advertising_contents_id", using: :btree

  create_table "applicant_arbeit_records", force: true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.integer  "from_year"
    t.integer  "from_month"
    t.integer  "to_year"
    t.integer  "to_month"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "applicant_arbeit_records", ["user_id"], name: "index_applicant_arbeit_records_on_user_id", using: :btree

  create_table "applicant_edu_records", force: true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.integer  "from_year"
    t.integer  "from_month"
    t.integer  "to_year"
    t.integer  "to_month"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "applicant_edu_records", ["user_id"], name: "index_applicant_edu_records_on_user_id", using: :btree

  create_table "applicant_statuses", force: true do |t|
    t.integer  "user_id"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "applicant_statuses", ["user_id"], name: "index_applicant_statuses_on_user_id", using: :btree

  create_table "applicant_wish_jobs", force: true do |t|
    t.integer  "user_id"
    t.string   "wish_job_name"
    t.integer  "wish_payment"
    t.string   "wish_placement"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "applicant_wish_jobs", ["user_id"], name: "index_applicant_wish_jobs_on_user_id", using: :btree

  create_table "applicants", force: true do |t|
    t.integer  "user_id"
    t.string   "belonging"
    t.string   "gakko_name"
    t.string   "address_pref"
    t.string   "railway_line"
    t.string   "address_city"
    t.integer  "applicant_status"
    t.string   "self_introduce"
    t.text     "self_apeal"
    t.integer  "monday_ok_flag"
    t.integer  "monday_ok_from_time"
    t.integer  "monday_ok_to_time"
    t.integer  "tuesday_ok_flag"
    t.integer  "tuesday_ok_from_time"
    t.integer  "tuesday_ok_to_time"
    t.integer  "wednesday_ok_flag"
    t.integer  "wednesday_ok_from_time"
    t.integer  "wednesday_ok_to_time"
    t.integer  "thursday_ok_flag"
    t.integer  "thursday_ok_from_time"
    t.integer  "thursday_ok_to_time"
    t.integer  "friday_ok_flag"
    t.integer  "friday_ok_from_time"
    t.integer  "friday_ok_to_time"
    t.integer  "saturday_ok_flag"
    t.integer  "saturday_ok_from_time"
    t.integer  "saturday_ok_to_time"
    t.integer  "sunday_ok_flag"
    t.integer  "sunday_ok_from_time"
    t.integer  "sunday_ok_to_time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "applicants", ["user_id"], name: "index_applicants_on_user_id", using: :btree

  create_table "categories", force: true do |t|
    t.string   "category"
    t.integer  "advertising_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "categories", ["advertising_id"], name: "index_categories_on_advertising_id", using: :btree

  create_table "identities", force: true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "images", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "imageable_id"
    t.string   "imageable_type"
  end

  add_index "images", ["imageable_id"], name: "index_images_on_imageable_id", using: :btree

  create_table "posts", force: true do |t|
    t.integer  "user_id"
    t.integer  "send_user_id"
    t.text     "content"
    t.integer  "anonymous",    default: 0
    t.string   "nickname"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "pushes", force: true do |t|
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pushes", ["user_id"], name: "index_pushes_on_user_id", using: :btree

  create_table "user_questions", force: true do |t|
    t.integer  "user_id"
    t.integer  "open_flag"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_questions", ["user_id"], name: "index_user_questions_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "gender"
    t.date     "birthday"
    t.integer  "ower_flag",              default: 0
    t.string   "family_name"
    t.string   "first_name"
    t.integer  "open",                   default: 0
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
