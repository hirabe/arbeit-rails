Rails.application.routes.draw do

  root to: 'users#top'
  
  devise_for :users, :controllers => { omniauth_callbacks: 'omniauth_callbacks' }
  # users
  match '/users/:id/finish_signup' => 'users#finish_signup', via: [:get, :patch], :as => :finish_signup
  match '/user/:id', to: 'users#show', via: 'get'
  match '/user/:id/edit_detail', to: 'users#edit_detail', via: 'get', :as => :edit_detail
  match '/users/:id/from_twitter', to: 'users#from_twitter', via: 'get', :as => :from_twitter
  match '/users/:id/save_from_twitter', to: 'users#save_from_twitter', via: 'patch', :as => :save_from_save
  
  resources :users do
  	
  end
  
  # applicants_record
  resources :applicant_records, only: [:create]
  
  # advertising
  resources :advertising, only: [:create, :new, :edit, :update]
  match '/advertising', to: 'advertising#top', via: 'get', :as => :advertising_top
  match '/advertising/search', to: 'advertising#search', via: 'post', :as => :advertising_search
  match '/advertising/search_result/:search', to: 'advertising#search_result', via: 'get', :as => :advertising_search_result
  
  # post
  resources :posts, only: [:create, :new, :edit, :update]
  match '/posts/send', to: 'posts#posts_send', via: 'get', :as => :posts_send
  
  namespace "applicants" do
  	
  end
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
